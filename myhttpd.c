/*
 ============================================================================
 Name        : myhttpd.c
 Author      : Aman Alok
 Version     :
 Copyright   :
 Description : myhttpd in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>



struct arguments{
	int debug;
	char *logFile;
	int port_no;
	char *rootdir;
	int queueing_time;
	int threadNum;
	char *schedPolicy;
}run_timeArgs;

struct requestQueue{
	struct clientRequestData* head;
	struct clientRequestData* tail;
	int capacity;
	int count;
	pthread_mutex_t lock;
	pthread_cond_t isNotEmpty;
	pthread_cond_t isNoFull;
}*acceptedQueue, *readyQueue;

struct clientRequestData{
	int socketFileDesc;
	char *ip_address;
	long fileSizeReq;
	char *fileTypeReq;
	int status_req;
	char *last_mod_time;
	char *timeAssignedExec;
	char *timeReceieved;
	char *firstLine;
	char *methodType;
	char *absPath;
	struct clientRequestData *next;
};

pthread_mutex_t lock_logging;
char* curr_work_directory;
FILE *log_file;

void printUsage(void);
void change_directory(char *dir);
struct requestQueue* initQueue(int cap);
void daemonize();
void logging();
void setup_server();
void *listen_incoming_request(void *server_sockfd);
void threadpool();
void *schedule_request();
void wait_queueing();
void *worker_task();
void parse_request(struct clientRequestData *request, char *requestString);
void enQueueReq(struct requestQueue* queue, struct clientRequestData* request);
struct clientRequestData* dequeueReq(struct requestQueue* queue);
void reqNode_del_frmQueue(struct requestQueue* queue, struct clientRequestData* request);
void doLogging(struct clientRequestData *request);
char *ret_gmt();


int main(int argc, char **argv){

	const char *optString= "dhl:p:r:t:n:s:";
	run_timeArgs.debug = 0;
	run_timeArgs.port_no = 8080;
	run_timeArgs.schedPolicy = "fcfs";
	run_timeArgs.rootdir = NULL;
	run_timeArgs.queueing_time = 60;
	int option;

	while((option=getopt(argc,argv,optString))!=-1){
			switch(option){
			case 'd':
				run_timeArgs.debug = 1;
				break;
			case 'h':
				printUsage();
				exit(1);
			case 'l':
				run_timeArgs.logFile=optarg;
				break;
			case 'p':
				run_timeArgs.port_no = atoi(optarg);
				if (run_timeArgs.port_no < 1024)
				{
				fprintf(stderr, "[error] Port number must be greater than or equal to 1024.\n");
				exit(1);
				}
				break;
			case 'r':
				run_timeArgs.rootdir = optarg;
				break;
			case 't':
				run_timeArgs.queueing_time = atoi(optarg);
				if (run_timeArgs.queueing_time < 1)
				{
				fprintf(stderr, "[error] queueing time must be greater than 0.\n");
				exit(1);
				}
				break;
			case 'n':
				run_timeArgs.threadNum = atoi(optarg);
				if (run_timeArgs.threadNum < 1)
				{
				fprintf(stderr, "[error] number of threads must be greater than 0.\n");
				exit(1);
				}
				break;
			case 's':
				 run_timeArgs.schedPolicy = optarg;
				 break;
			default:
				printUsage();
				exit(1);
			}
		}

	if(run_timeArgs.debug != 1){
		daemonize();
	}

	if(run_timeArgs.rootdir != NULL){
		change_directory(run_timeArgs.rootdir);
	}
	curr_work_directory = getcwd(NULL, 0);

	if(run_timeArgs.debug!=1)
		acceptedQueue = initQueue(-1);
	else
		acceptedQueue = initQueue(1);

	readyQueue = initQueue(1);

	setup_server();
	return 0;
}

char *ret_gmt(){
	time_t tim = time(NULL);
	return asctime(gmtime(&tim));
}

void *worker_task(){

	int client_socket_fd;
	//int e;
	char one_line[1024];
	FILE *fpntr;

	while (1) {
		struct clientRequestData *req = dequeueReq(readyQueue);
		client_socket_fd= req->socketFileDesc;
		int rStatus = req->status_req;
		if (rStatus == 404){
				write(client_socket_fd,"HTTP/1.0 404 Not Found\r\n",25);
				write(client_socket_fd,"Content-Type: text/html\r\n",25);
				write(client_socket_fd,"\n\n",3);
				write(client_socket_fd,"Resource Not Found 404",22);
				close(client_socket_fd);

		}
		else if(rStatus==200){
			if(strcmp(req->methodType,"GET")==0){
				if(strcmp(req->fileTypeReq,"directory")!=0){
					fpntr=fopen(req->absPath,"r");
					char *content_type="text/plain";
					if(strcmp(req->fileTypeReq,"html")==0)
						content_type="text/html";
					else if(strcmp(req->fileTypeReq,"jpeg")==0)
						content_type="image/jpeg";
					else if(strcmp(req->fileTypeReq,"gif")==0)
						content_type="image/gif";
					fseek(fpntr, 0, SEEK_END);
					int size = ftell(fpntr);
					rewind(fpntr);
					char s[20];
					sprintf(s,"%d",size);
					write(client_socket_fd, "HTTP/1.0 200 OK\n",16);
					write(client_socket_fd,"Connection: close\n",19);
					write(client_socket_fd, "Date: ",7);
					char *tim=ret_gmt();
					write(client_socket_fd, tim,strlen(tim));
					write(client_socket_fd, "Server: ",9);
					write(client_socket_fd,"myhttpd\n",8);
					write(client_socket_fd,"Last-Modified: ",14);
					write(client_socket_fd,req->last_mod_time,strlen(req->last_mod_time));
					write(client_socket_fd,"Content-Length: ",17);
					write(client_socket_fd,s,strlen(s));
					write(client_socket_fd, "\nContent-Type: ",16);
					write(client_socket_fd, content_type,strlen(content_type));
					write(client_socket_fd, "\n\n",3);
					while (fgets(one_line,1024,fpntr)!=NULL)
					write(client_socket_fd,one_line,strlen(one_line));
					close(client_socket_fd);
		}
		else if(strcmp(req->fileTypeReq,"directory")==0){
			int abs_path_length = strlen(req->absPath);
			int cos = strlen("index.html");
			int path_length = abs_path_length + cos + 1;
			char *index_file_path = (char *) malloc(path_length * sizeof(char));
			strcpy(index_file_path,req->absPath);
			strcat(index_file_path,"index.html");
			FILE *fp= fopen(index_file_path,"r");
			if(fp==NULL){
				chdir(req->absPath);
				FILE *fpntr= popen("ls -lrt","r");
				chdir(curr_work_directory);
				int sz=0 ;
				while (fgets(one_line,1024,fpntr)){
					sz = sz + strlen(one_line);
					write(client_socket_fd,one_line,strlen(one_line));
				}
				req->fileSizeReq=sz;
				close(client_socket_fd);
			}
			else{
				fseek(fp, 0, SEEK_END);
				//char si[20];
				int size = ftell(fp);
				rewind(fp);
				char sz[20];
				sprintf(sz,"%d",size);
				req->fileSizeReq=size;
				char *contentType="text/html";
				char *cTime;
				write(client_socket_fd, "HTTP/1.0 200 OK\n",16);
				write(client_socket_fd,"Connection: close\n",19);
				write(client_socket_fd, "Date: ",7);
				cTime=ret_gmt();
				write(client_socket_fd, cTime,strlen(cTime));
				write(client_socket_fd, "Server: ",9);
				write(client_socket_fd,"myhttpd\n",8);
				write(client_socket_fd,"Last-Modified: ",15);
				write(client_socket_fd,req->last_mod_time,strlen(req->last_mod_time));
				write(client_socket_fd,"Content-Length: ",17);
				write(client_socket_fd,sz,strlen(sz));
				write(client_socket_fd, "\nContent-type: ",16);
				write(client_socket_fd, contentType,strlen(contentType));
				write(client_socket_fd, "\n\n",3);
				while (fgets(one_line,1024,fp))
				write(client_socket_fd,one_line,strlen(one_line));
				close(client_socket_fd);
			}
		}
			}
		}
		if(strcmp(req->methodType,"HEAD")==0){
			char *content_type="text/plain";
			//char *tim;
			if(strcmp(req->fileTypeReq,"html")==0)
				content_type="text/html";
			else if(strcmp(req->fileTypeReq,"jpeg")==0)
				content_type="image/jpeg";
			else if(strcmp(req->fileTypeReq,"gif")==0)
				content_type="image/gif";
			write(client_socket_fd, "HTTP/1.0 200 OK\n",16);
			write(client_socket_fd, "Date: ",7);
			char *tim = ret_gmt();
			write(client_socket_fd, tim,strlen(tim));
			write(client_socket_fd, "Server: ",8);
			write(client_socket_fd,"myhttpd",7);
			write(client_socket_fd,"\n",2);
			write(client_socket_fd,"Last-Modified:",14);
			write(client_socket_fd,req->last_mod_time,strlen(req->last_mod_time));
			write(client_socket_fd, "Content-type:",13);
			write(client_socket_fd, content_type,strlen(content_type));
			write(client_socket_fd, "\n\n",3);
			close(client_socket_fd);
		}

		doLogging(req);
	}
}
void doLogging(struct clientRequestData *request){
	pthread_mutex_lock(&lock_logging);
	if(log_file!=NULL){
		char* queued_time = strtok(request->timeReceieved, "\n");
		char* sched_time = strtok(request->timeAssignedExec, "\n");
		char* fline = strtok(request->firstLine, "\n");
		fprintf(log_file, "%s - [%s] [%s]\n%s%d %ld\n", request->ip_address,
				queued_time, sched_time, fline, request->status_req, request->fileSizeReq);
		fflush(log_file);
	}

	pthread_mutex_unlock(&lock_logging);
}

void reqNode_del_frmQueue(struct requestQueue* queue, struct clientRequestData* request) {
	pthread_mutex_lock(&queue->lock);
	struct clientRequestData* flag = queue->head;
	if (queue->head == request) {
		  queue->head = queue->head->next;
		  if(queue->head==NULL)  queue->tail = NULL;

	} else {
		while (flag != NULL && flag->next != request) {
			flag = flag->next;
		}
		flag->next = flag->next->next;
	}
	queue->count--;

	pthread_cond_signal(&queue->isNoFull);
	pthread_mutex_unlock(&queue->lock);
}

struct clientRequestData* dequeueReq(struct requestQueue* queue){
	  struct clientRequestData* item;
	  pthread_mutex_lock(&queue->lock);
	  while(queue->count == 0){
		  pthread_cond_wait(&queue->isNotEmpty,&queue->lock);
	  }

	  if( queue->head==NULL){
	      return NULL;
	  }
	  item = queue->head;
	  queue->head = queue->head->next;

	  if(queue->head==NULL ){
		  queue->tail = NULL;
	  }
	  queue->count--;
	  pthread_cond_signal(&queue->isNoFull);
	  pthread_mutex_unlock(&queue->lock);
	  return item;
}

void enQueueReq(struct requestQueue* queue, struct clientRequestData* request){
	  pthread_mutex_lock(&queue->lock);
	  if(queue->capacity != -1 ){
		  while(queue->count == queue->capacity){
			pthread_cond_wait(&queue->isNoFull,&queue->lock);
		  }
	  }

	  if( queue->head ==NULL){
	      queue->head = queue->tail = request;
	  }
	  else{
	      queue->tail->next = request;
	      queue->tail = request;
	    }
	  queue->count++;
	  pthread_cond_signal(&queue->isNotEmpty);
	  pthread_mutex_unlock(&queue->lock);
}

void parse_request(struct clientRequestData *request,char *requestString){
	char *first_line = strtok(requestString,"\n");
	request->firstLine=first_line;
	char *f_line = strdup(first_line);
	char *request_type = strtok(f_line," ");
	request->methodType=request_type;
	char *file_path = strtok(NULL," ");
	char *usr_find = strchr(file_path,'~');

	if(usr_find!=NULL){
		usr_find=usr_find + 1;
		char * usr;
		usr = strtok(strdup(usr_find),"/");
		char *d_path;
		d_path= strdup(usr_find);
		int usr_len = strlen(usr);
		d_path= d_path + usr_len + 1;
		int d_path_len = strlen(d_path);
		int length = strlen("/home/") + strlen("/myhttpd/") + usr_len + d_path_len + 1;
		char *file_path_n= (char *) malloc(length * sizeof(char));
		strcpy(file_path_n,"/home/");
		strcat(file_path_n,usr);
		strcat(file_path_n,"/myhttpd/");
		chdir(file_path_n);
		strcat(file_path_n,d_path);
		file_path=d_path;
		request->absPath=file_path_n;
	}
	else{
		int file_path_len = strlen(file_path);
		int path_len = strlen(curr_work_directory) + file_path_len + 1;
		char *entire_file_path=(char *) malloc(path_len * sizeof(char));
		strcpy(entire_file_path,curr_work_directory);
		strcat(entire_file_path,file_path);
		request->absPath = entire_file_path;
		file_path = file_path+1;
	}

	int file_directory = 0;
	file_directory = open(file_path,O_RDONLY,0);
	struct stat stats;
	time_t last_modified = stats.st_mtim.tv_sec;

	chdir(curr_work_directory);

	if(file_directory>=0){
		request->status_req=200;
		if (fstat(file_directory, &stats) < 0) {
			perror("fstat failed");
			exit(1);
		}
		request->last_mod_time = strdup(asctime(gmtime(&last_modified)));

		if (S_ISDIR(stats.st_mode)){
			request->fileTypeReq = "directory";
			request->fileSizeReq = 0;
			if(close(file_directory) < 0)
				perror("close failed");
		}
		else {
			char *file_extension = strrchr(file_path, '.');

				if (!file_extension || file_extension == file_path)
					file_extension="";
				else
					file_extension = file_extension + 1;

				request->fileTypeReq = strdup(file_extension);

			if(strcmp("GET",request->methodType)==0){
				request->fileSizeReq=stats.st_size;
			}
			else{
				request->fileSizeReq = 0;
				if(close(file_directory) < 0)
					perror("close failed");
			}
		}

	}
	else{
		request->status_req=404;
		request->fileSizeReq=0;
		request->fileTypeReq="";
		request->last_mod_time = "";
	}
}

void wait_queueing(){
	if(run_timeArgs.debug != 1)
		sleep(run_timeArgs.queueing_time);
}

void *schedule_request(){

	struct clientRequestData *flag, *item = NULL;
	long min_file_size = -1;
	time_t t_sched = time(NULL);
	wait_queueing();
	while (1) {
		pthread_mutex_lock(&acceptedQueue->lock);
		while(acceptedQueue->head == NULL){
					pthread_cond_wait(&acceptedQueue -> isNotEmpty,&acceptedQueue->lock);
		}
		flag = acceptedQueue->head;
		item = flag;

		if (strcmp(run_timeArgs.schedPolicy, "sjf") == 0) {
			while (flag != NULL) {
				if (min_file_size == -1) {
					min_file_size = flag->fileSizeReq;
				}
				if (min_file_size > flag->fileSizeReq) {
					min_file_size = flag->fileSizeReq;
					item = flag;
				}
				flag = flag->next;
			}
		}
		pthread_mutex_unlock(&acceptedQueue->lock);
		if (item != NULL) {
			reqNode_del_frmQueue(acceptedQueue, item);
			item->timeAssignedExec = strdup(asctime(gmtime(&t_sched)));
			enQueueReq(readyQueue, item);
		}
	}

}

void threadpool() {
	int i;
	int num_threads;

	if(run_timeArgs.debug==1)
		num_threads=1;
	else if(run_timeArgs.threadNum>0)
		num_threads=run_timeArgs.threadNum;
	else
		num_threads=4;

	pthread_t scheduler_thread;

	pthread_t *worker_threads = malloc(num_threads * sizeof(pthread_t));

	pthread_create(&scheduler_thread, NULL, schedule_request, NULL);

	for (i = 0; i < num_threads; i++) {
		pthread_create(worker_threads + i, NULL, worker_task,NULL);
	}
}

void *listen_incoming_request(void *server_sockfd){
	struct sockaddr_in in_addr;
	socklen_t inc_addr_size;
	int client_sockfd;
	char *ipaddr_client;
	char clientRequestBuf[1024];
	time_t receiveTime = time(NULL);
	int serve_fd = *((int*) server_sockfd);

    if (listen(serve_fd, 5) == -1) {
        perror("listen");
        exit(1);
    }
    threadpool();

	while(1){
			inc_addr_size = sizeof(in_addr);
        	client_sockfd = accept(serve_fd, (struct sockaddr *)&in_addr, &inc_addr_size);
        	if (client_sockfd == -1) {
            	perror("accept");
            	continue;
        	}
        struct clientRequestData *clientRequest=malloc(1* sizeof(*clientRequest));
		clientRequest->next=NULL;
		clientRequest->socketFileDesc=client_sockfd;
		ipaddr_client=inet_ntoa(in_addr.sin_addr);
		clientRequest->ip_address=strdup(ipaddr_client);

    	if(recv(client_sockfd, clientRequestBuf, sizeof(clientRequestBuf), 0) == -1){
    		perror("receive");
    	}

		parse_request(clientRequest,clientRequestBuf);
		clientRequest->timeReceieved=strdup(asctime(gmtime(&receiveTime)));
		enQueueReq(acceptedQueue, clientRequest);
	}
}

void setup_server(){

	if(run_timeArgs.debug==1 || run_timeArgs.logFile!=NULL)
		logging();
	pthread_t listenReq;
	int sock_fd;
	unsigned server_len, client_len;
	struct sockaddr_in servinfo;
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(sock_fd == -1){
		perror("server: socket");
	}
	servinfo.sin_family = AF_INET;
	servinfo.sin_addr.s_addr = htons(INADDR_ANY);
	servinfo.sin_port=htons(run_timeArgs.port_no);

	if(bind(sock_fd, (struct sockaddr *) &servinfo, sizeof(servinfo)) == -1){
		close(sock_fd);
		perror("server: bind");
	}

	pthread_create (&listenReq, NULL, listen_incoming_request, (void*)&sock_fd);
	pthread_join(listenReq,NULL);
}

struct requestQueue* initQueue(int cap){
	struct requestQueue* queue = malloc( 1 * sizeof(*queue));
	queue->head = queue->tail = NULL;
	queue->capacity = cap;
	queue->count = 0;
	pthread_cond_init(&queue->isNotEmpty, NULL);
	pthread_cond_init(&queue->isNoFull, NULL);
	pthread_mutex_init(&queue->lock, NULL);
	return queue;
}

void logging(){
	if(run_timeArgs.debug == 1){
		log_file = stdout;
	}
	else{
		int cwd_len = strlen(curr_work_directory);
		int logfile_len = strlen(run_timeArgs.logFile);
		int path_len = cwd_len + logfile_len;
		char* log_file_path = (char*) malloc(path_len * sizeof(char));
		strcpy(log_file_path, curr_work_directory);
		strcat(log_file_path,"/");
		strcat(log_file_path, run_timeArgs.logFile);
		log_file=fopen(log_file_path,"a+");
	}
	pthread_mutex_init(&lock_logging, NULL);
}

void daemonize(){
	int id_process = fork();
	int session_id = 0;
	if(id_process < 0){
		printf("Fork Failed!!!\n");
		exit(1);
	}
	if(id_process == 0){
		session_id = setsid();
		umask(0);
	}
	else{
		exit(0);
	}
}

void change_directory(char *dir){
	int i = chdir(dir);
	if(i < 0){
		printf("Could not change directory!!!\n");
	}
}

void printUsage(void)
{
fprintf(stderr, "Usage: myhttpd [−d] [−h] [−l file] [−p port] [−r dir] [−t time] [−n thread_num] [−s sched]\n");

fprintf(stderr,
"\t−d : Enter debugging mode. That is, do not daemonize, only accept\n"
"\tone connection at a time and enable logging to stdout. Without\n"
"\tthis option, the web server should run as a daemon process in the\n"
"\tbackground.\n"
"\t−h : Print a usage summary with all options and exit.\n"
"\t−l file : Log all requests to the given file. See LOGGING for\n"
"\tdetails.\n"
"\t−p port : Listen on the given port. If not provided, myhttpd will\n"
"\tlisten on port 8080.\n"
"\t−r dir : Set the root directory for the http server to dir.\n"
"\t−t time : Set the queuing time to time seconds. The default should\n"
"\tbe 60 seconds.\n"
"\t−n thread_num : Set number of threads waiting ready in the execution thread pool to\n"
"\tthreadnum. The d efault should be 4 execution threads.\n"
"\t−s sched : Set the scheduling policy. It can be either FCFS or SJF.\n"
"\tThe default will be FCFS.\n"
);
}
